﻿using RosSharp.RosBridgeClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agvlaser
{
    public class DataStructClass
    {
        /// <summary>
        /// 站点
        /// </summary>
        public class Station
        { 
            /// <summary>
            /// 站点描述
            /// </summary>
            public string StationDes
            {
                get; set;
            }
            /// <summary>
            /// 机器人角度
            /// </summary>
            public double StationInfoAngel
            {
                get; set;
            }
            /// <summary>
            /// 站点停留时间
            /// </summary>
            public int StationInfoStopTime
            {
                get; set;
            }

            /// <summary>
            /// 机器人姿态
            /// </summary>
            public GeometryPose Pose = new GeometryPose();
        }

        /// <summary>
        /// 路径
        /// </summary>
        public class Route
        {
            /// <summary>
            /// 路径编号
            /// </summary>
            public int RouteNum
            {
                get; set;
            }
            /// <summary>
            /// 路径描述
            /// </summary>
            public string RouteDes
            {
                get; set;
            }
            /// <summary>
            /// 站点总数
            /// </summary>
            public int StationTotal
            {
                get; set;
            }
            public List<Station> stations = new List<Station>();
        }

        /// <summary>
        /// 系统配置
        /// </summary>
        public class SystemConfig
        {
            /// <summary>
            /// 机器人IP
            /// </summary>
            public string RobotIp
            {
                get; set;
            }
            /// <summary>
            /// 界面配置
            /// </summary>
            public ViewConfig ViewConfig = new ViewConfig();
            /// <summary>
            /// 路径设置
            /// </summary>
            public RouteConfig RouteConfig = new RouteConfig();
        }

        /// <summary>
        /// 视图配置
        /// </summary>
        public class ViewConfig
        {
            /// <summary>
            /// 缩放比例
            /// </summary>
            public float Scale
            {
                get; set;
            }
            /// <summary>
            /// 原点坐标
            /// </summary>
            public Point OriginPoint
            {
                get; set;
            }
        }

        /// <summary>
        /// 路径配置
        /// </summary>
        public class RouteConfig
        {
            /// <summary>
            /// 当前路径号
            /// </summary>
            public int RouteNow
            {
                get; set;
            }
            /// <summary>
            /// 当前站点号
            /// </summary>
            public int StationNow
            {
                get; set;
            }
        }

    }
}
