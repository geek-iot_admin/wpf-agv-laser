﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RosSharp.RosBridgeClient
{
    public static class MessageList
    {
        public static class geometry_msgs
        {
            public static string Twist
            {
                get { return "geometry_msgs/Twist"; }
            }
            public static string Accel
            {
                get { return "geometry_msgs/Accel"; }
            }
            public static string Vector3
            {
                get { return "geometry_msgs/Vector3"; }
            }
            public static string PoseWithCovariance
            {
                get { return "geometry_msgs/PoseWithCovariance"; }
            }
            public static string PoseWithCovarianceStamped
            {
                get { return "geometry_msgs/PoseWithCovarianceStamped"; }
            }
            public static string TwistWithCovariance
            {
                get { return "geometry_msgs/TwistWithCovariance"; }
            }
            public static string Pose
            {
                get { return "geometry_msgs/Pose"; }
            }
            public static string PoseStamped
            {
                get { return "geometry_msgs/PoseStamped"; }
            }
            public static string Point
            {
                get { return "geometry_msgs/Point"; }
            }
            public static string Quaternion
            {
                get { return "geometry_msgs/Quaternion"; }
            }
            public static string Transform
            {
                get { return "geometry_msgs/Transform"; }
            }
            public static string TransformStamped
            {
                get { return "geometry_msgs/TransformStamped"; }
            }
        }

        public static class std_msgs
        {
            public static string String
            {
                get { return "std_msgs/String"; }
            }
            public static string Header
            {
                get { return "std_msgs/Header"; }
            }
            public static string Time
            {
                get { return "std_msgs/Time"; }
            }
        }
        public static class sensor_msgs
        {
            public static string JointState
            {
                get { return "sensor_msgs/JointState"; }
            }
            public static string Joy
            {
                get { return "sensor_msgs/Joy"; }
            }
            public static string PointCloud2
            {
                get { return "sensor_msgs/PointCloud2"; }
            }
            public static string PointField
            {
                get { return "sensor_msgs/PointField"; }
            }
            public static string Image
            {
                get { return "sensor_msgs/Image"; }
            }
            public static string CompressedImage
            {
                get { return "sensor_msgs/CompressedImage"; }
            }
        }
        public static class nav_msgs
        {
            public static string Odometry
            {
                get { return "nav_msgs/Odometry"; }
            }
            public static string MapMetaData
            {
                get { return "nav_msgs/MapMetaData"; }
            }
            public static string OccupancyGrid
            {
                get { return "nav_msgs/OccupancyGrid"; }
            }
            public static string Path
            {
                get { return "nav_msgs/Path"; }
            }
        }
        public static class tf2_msgs
        {
            public static string TFMessage
            {
                get { return "tf2_msgs/TFMessage"; }
            }
        }

        public static class move_base_msgs
        {
            public static string MoveBaseActionResult
            {
                get { return "move_base_msgs/MoveBaseActionResult"; }
            }
            public static string MoveBaseResult
            {
                get { return "move_base_msgs/MoveBaseResult"; }
            }
        }
        public static class actionlib_msgs
        {
            public static string GoalID
            {
                get { return "actionlib_msgs/GoalID"; }
            }
            public static string GoalStatus
            {
                get { return "actionlib_msgs/GoalStatus"; }
            }
        }

    }
}
