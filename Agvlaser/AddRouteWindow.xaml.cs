﻿using Agvlaser.Sqlite;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static Agvlaser.BindHelper;
using static Agvlaser.DataStructClass;

namespace Agvlaser
{
    /// <summary>
    /// AddRouteWindow.xaml 的交互逻辑
    /// </summary>
    public partial class AddRouteWindow : Window
    {
        public AddRouteWindow()
        {
            InitializeComponent();
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //加载当前路径信息
            tbRouteNum.Text = GlobalVar.routeInfo.RouteNum.ToString();
            tbRouteDes.Text = GlobalVar.routeInfo.RouteDes.ToString();
        }
        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        int routeNum;
        private void ButtonSubmit_Click(object sender, RoutedEventArgs e)
        {
            //判断路径号是否为数字
            if (IsNumber(tbRouteNum.Text))
            {
                routeNum = int.Parse(tbRouteNum.Text);
                string routeDes = tbRouteDes.Text;
                //读取路径信息
                SqliteHelper sql = new SqliteHelper();
                string str = sql.ExecuteScalar(string.Format("select route from route where num={0}", routeNum)) as string;
                //存在，提示进行修改
                if (str != null)
                {
                    MessageBox.Show("您添加的路径已经存在，您可以进行编辑或删除后再试！");
                }
                else
                //不存在，存储
                {
                    //初始化路径信息
                    Route routOject = new Route();
                    routOject.RouteNum = routeNum;
                    routOject.RouteDes = tbRouteDes.Text;
                    str = JsonConvert.SerializeObject(routOject, Formatting.Indented);
                    //存储
                    int res = sql.ExecuteNonQuery(string.Format("insert into route (num,route) VALUES ({0},'{1}')", routeNum, str));
                    if (res > 0)
                    {
                        MessageBox.Show("添加成功！");
                        GlobalVar.systemConfig.RouteConfig.RouteNow = routeNum;
                        GlobalVar.systemConfig.RouteConfig.StationNow = 0;
                        this.Close();
                    }
                    else
                        MessageBox.Show("添加失败！");
                }
            }
            else
            {
                MessageBox.Show("路径号必须为数字！");
            }
        }
        bool vaild = false;
        private void tbRouteNum_TextChanged(object sender, TextChangedEventArgs e)
        {
            //判断路径号是否为数字
            if (IsNumber(tbRouteNum.Text))
            {
                routeNum = int.Parse(tbRouteNum.Text);
                //读取路径信息
                SqliteHelper sql = new SqliteHelper();
                string str = sql.ExecuteScalar(string.Format("select route from route where num={0}", routeNum)) as string;
                //存在，提示进行修改
                if (str != null)
                {
                    //更新界面描述信息
                    Route route = JsonConvert.DeserializeObject<Route>(str);
                    tbRouteDes.Text = route.RouteDes;
                    tbRouteNum.Background = Brushes.Yellow;
                    vaild = false;
                }
                else
                {
                    tbRouteDes.Text = "该路径编号为空，可以创建，请单击此处填写描述信息！";
                    tbRouteNum.Background = Brushes.Green;
                    vaild = true;
                }
            }
        }
        private void tbRouteDes_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (vaild)
            {
                tbRouteDes.Text = "";
            }
        }

        /// <summary>
        /// 判断字符串是否是数字
        /// </summary>
        public static bool IsNumber(string s)
        {
            if (string.IsNullOrWhiteSpace(s)) return false;
            const string pattern = "^[0-9]*$";
            Regex rx = new Regex(pattern);
            return rx.IsMatch(s);
        }


    }
}
