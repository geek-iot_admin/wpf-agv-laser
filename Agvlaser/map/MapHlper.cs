﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Agvlaser.map
{
    public class MapHlper
    {
        //默认地图位置
        const string MapPath = @"C:\ProgramData\Agvlaser\map.bin";
        /// <summary>
        /// 存储地图
        /// </summary>
        /// <param name="data">地图原始数据</param>
        /// <param name="path">目标路径</param>
        public void SaveMap(SByte[] data, string path = MapPath)
        {
            //存储地图
            FileStream fs = new FileStream(path, FileMode.OpenOrCreate);
            BinaryWriter binWriter = new BinaryWriter(fs);
            byte[] bt = new byte[data.Length];
            for (int i = 0; i < data.Length; i++)
            {
                switch (data[i])
                {
                    //空白
                    case 0:
                        bt[i] = 0;
                        break;
                    //障碍物
                    case 100:
                        bt[i] = 1;
                        break;
                    //未知
                    case -1:
                        bt[i] = 2;
                        break;
                    default:
                        break;
                }
            }
            binWriter.Write(bt, 0, bt.Length);
            binWriter.Close();
            fs.Close();
        }

        /// <summary>
        /// 获取地图原始数据
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public byte[] GetMapData(string path = MapPath)
        {
            //加载地图
            byte[] bBuffer;
            FileStream fs = new FileStream(path, FileMode.Open);
            BinaryReader binReader = new BinaryReader(fs);

            bBuffer = new byte[fs.Length];
            binReader.Read(bBuffer, 0, (int)fs.Length);

            binReader.Close();
            fs.Close();
            return bBuffer;
        }

        public Bitmap GetMap(string path = MapPath)
        {
            byte[] bBuffer = GetMapData();
            //panel1.BackColor = Color.Black;
            Bitmap im = new Bitmap(600, 600);
            Graphics g = Graphics.FromImage(im);
            uint width = 576;
            uint height = 544;
            int wStart = 0;
            int hStart = 0;
            int pointWidth = 2;
            //绕任意点旋转
            //g.TranslateTransform(300 * pointWidth, 300 * pointWidth);
            //g.RotateTransform(0);
            //g.TranslateTransform(-300 * pointWidth, -300 * pointWidth);

            //绘制无效区域
            g.FillRectangle(Brushes.LightGray, wStart * pointWidth, hStart * pointWidth, width * pointWidth, height * pointWidth);
            //像素点宽度
            for (int h = wStart; h < 544; h++)
            {
                for (int w = wStart; w < 576; w++)
                {

                    if (bBuffer[width * h + w] == 1)
                    {
                        g.FillRectangle(Brushes.Blue, (w - wStart) * pointWidth, (h - hStart) * pointWidth, pointWidth, pointWidth);
                    }
                    if (bBuffer[width * h + w] == 0)
                    {
                        g.FillRectangle(Brushes.Gray, (w - wStart) * pointWidth, (h - hStart) * pointWidth, pointWidth, pointWidth);
                    }
                }
            }

            return im;
        }

        [DllImport("gdi32.dll", SetLastError = true)]
        private static extern bool DeleteObject(IntPtr hObject);
        /// <summary>
        /// 从bitmap转换成ImageSource
        /// </summary>
        /// <param name="icon"></param>
        /// <returns></returns>
        public static System.Windows.Media.ImageSource ChangeBitmapToImageSource(Bitmap bitmap)
        {
            //Bitmap bitmap = icon.ToBitmap();
            IntPtr hBitmap = bitmap.GetHbitmap();
            System.Windows.Media.ImageSource wpfBitmap = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                hBitmap,
                IntPtr.Zero,
                Int32Rect.Empty,
                System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());
            if (!DeleteObject(hBitmap))

            {
                throw new System.ComponentModel.Win32Exception();
            }
            return wpfBitmap;

        }
    }
}
