﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Agvlaser
{
    public class BindHelper
    {
        public abstract class BindableObject : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;
            protected void OnPropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
            }

            protected void SetProperty<T>(ref T item, T value, [CallerMemberName] string propertyName = null)
            {
                if (!EqualityComparer<T>.Default.Equals(item, value))
                {
                    item = value;
                    OnPropertyChanged(propertyName);
                }
            }
        }

        #region Bottom 底部信息栏
        public class BottomInfoBound : BindableObject
        {
            private string _RobotAngle = "";
            private string _ViewScale = "";
            private string _OriginPoint = "";
            private string _WordAxis = "";
            private string _ViewAxis = "";

            /// <summary>
            /// 机器人当前角度
            /// </summary>
            public string RobotAngle
            {
                get { return _RobotAngle; }
                set { SetProperty(ref _RobotAngle, value); }
            }
            /// <summary>
            /// 视图缩放比例
            /// </summary>
            public string ViewScale
            {
                get { return _ViewScale; }
                set { SetProperty(ref _ViewScale, value); }
            }
            /// <summary>
            /// 原点坐标
            /// </summary>
            public string OriginPoint
            {
                get { return _OriginPoint; }
                set { SetProperty(ref _OriginPoint, value); }
            }
            /// <summary>
            /// 世界坐标
            /// </summary>
            public string WordAxis
            {
                get { return _WordAxis; }
                set { SetProperty(ref _WordAxis, value); }
            }
            /// <summary>
            /// 视图坐标
            /// </summary>
            public string ViewAxis
            {
                get { return _ViewAxis; }
                set { SetProperty(ref _ViewAxis, value); }
            }
        }
        #endregion

        #region 路径编辑
        public class RouteEditBound : BindableObject
        {
            private string _RouteNow = "";
            private string _RouteDes = "";
            private string _StationTotal = "";
            private string _StationNow = "";
            private string _StationDes = "";
            private string _StationInfoX = "";
            private string _StationInfoY = "";
            private string _StationInfoAngel = "";
            private string _StationInfoStopTime = "";

            /// <summary>
            /// 当前路径
            /// </summary>
            public string RouteNow
            {
                get { return _RouteNow; }
                set { SetProperty(ref _RouteNow, value); }
            }
            /// <summary>
            /// 路径描述
            /// </summary>
            public string RouteDes
            {
                get { return _RouteDes; }
                set { SetProperty(ref _RouteDes, value); }
            }
            /// <summary>
            /// 站点总数
            /// </summary>
            public string StationTotal
            {
                get { return _StationTotal; }
                set { SetProperty(ref _StationTotal, value); }
            }
            /// <summary>
            /// 当前站点
            /// </summary>
            public string StationNow
            {
                get { return _StationNow; }
                set { SetProperty(ref _StationNow, value); }
            }
            /// <summary>
            /// 站点描述
            /// </summary>
            public string StationDes
            {
                get { return _StationDes; }
                set { SetProperty(ref _StationDes, value); }
            }
            /// <summary>
            /// X轴坐标
            /// </summary>
            public string StationInfoX
            {
                get { return _StationInfoX; }
                set { SetProperty(ref _StationInfoX, value); }
            }
            /// <summary>
            /// Y轴坐标
            /// </summary>
            public string StationInfoY
            {
                get { return _StationInfoY; }
                set { SetProperty(ref _StationInfoY, value); }
            }
            /// <summary>
            /// 机器人角度
            /// </summary>
            public string StationInfoAngel
            {
                get { return _StationInfoAngel; }
                set { SetProperty(ref _StationInfoAngel, value); }
            }
            /// <summary>
            /// 站点停留时间
            /// </summary>
            public string StationInfoStopTime
            {
                get { return _StationInfoStopTime; }
                set { SetProperty(ref _StationInfoStopTime, value); }
            }
        }
        #endregion
    }
}
