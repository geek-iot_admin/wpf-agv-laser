﻿using RosSharp.RosBridgeClient;
using System.Collections.Generic;
using static Agvlaser.DataStructClass;

namespace Agvlaser
{
    public class GlobalVar
    {
        //Socket客户端
        public static RosSocket rosSocket;
        //静态栅格地图
        public static NavigationOccupancyGrid mapGrid;
        //话题列表
        public static Dictionary<string, string> TocpicList = new Dictionary<string, string>();
        //系统配置
        public static SystemConfig systemConfig = new SystemConfig();
        //当前路径信息
        public static Route routeInfo = new Route();

        //鼠标当前功能功能
        public enum MouseFunctionEnum
        {
            /// <summary>
            /// 空闲
            /// </summary>
            None,
            /// <summary>
            /// 设置机器人位置
            /// </summary>
            ResetRobot,
            /// <summary>
            /// 发布机器人目标位置
            /// </summary>
            SetTargetPose,
            /// <summary>
            /// 移动视图
            /// </summary>
            MoveView,
            /// <summary>
            /// 缩放视图
            /// </summary>
            ZoomView,
            /// <summary>
            /// 路径编辑模式，左键移动位置，右键调整方向
            /// </summary>
            RouteEdit,
        }
        public static MouseFunctionEnum mouseFunction = MouseFunctionEnum.None;
        //鼠标左键按下，记录按下的位置
        public static System.Windows.Point mouseLeftBtnDownPoint = new System.Windows.Point();
        //右键按下
        public static System.Windows.Point mouseRightBtnDownPoint = new System.Windows.Point();
        //地图尺寸
        public static System.Drawing.Size MapSize;

        //机器人状态
        public class Robot
        {
            //已连接
            public static bool Connected = false;
        }
    }
}
